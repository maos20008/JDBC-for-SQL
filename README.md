# JDBC-for-SQL
A simple User login demo implementing a Java Database Connectivity (JDBC) driver for use with a SQL Server.


![jdbc-6](https://user-images.githubusercontent.com/18353476/37892010-3f5bd472-308b-11e8-9ef8-e24c331e30ce.jpg)


This project example can be expanded upon by ulitizing the Microsoft JDBC Drive.

[Microsoft JDBC Driver for SQL Server and Azure SQL Database](https://docs.microsoft.com/en-us/sql/connect/jdbc/microsoft-jdbc-driver-for-sql-server)

[Microsoft JDBC Driver for SQL Server Team Blog ](https://blogs.msdn.microsoft.com/jdbcteam/)

[Microsoft SQL Server JDBC for Linux](https://www.mathworks.com/help/database/ug/microsoft-sql-server-jdbc-linux.html)
