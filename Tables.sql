You a database have of 10 teams that are playing 5 different types of games head to head. In this instance of Tables we need to create 1 for the teams 
and 1 for the games and a reference table to list the results of each game.

TABLE teams (
     tid
     tname
     tstadium
etc...
);

TABLE game (
     gid
     gname
     grules
etc...
);

TABLE match (
    gid
    tid1
    tid2
    winner
    score
etc...
);
===================================
Individual Player Stats
===================================
TABLE player (
     pid
     pfname
     pname
);

TABLE opponent (
     oid
     oname
     odate
     olocation
);

TABLE stats (
     sid
     pid
     oid
     spoints
     sassists
     srebounds
);

====================================================
The data in the tables should look like this
====================================================
Table player:
1, Michael, Jordan
2, Scottie, Pippen
3, Dennis, Rodman


Table spponent
1, Oakland, 20/11/00, home
2, Cleveland, 23/11/00, away


Table stats
1, 1, 1, 40, 7, 4
2, 2, 1, 26, 5, 6
3, 3, 1, 9, 3, 14
4, 1, 2, 49, 3, 6
5, 2, 2, 17, 7, 2
6, 3, 2, 11, 1, 12